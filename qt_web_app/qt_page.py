"""
QTrobot web config
"""


class Page:
    # Store the page title in a class attribute
    title = 'QTrobot'
    status_text = "alive"

    def setStatusText(self, text):
        self.status_text = text;

    def header(self):
        return '''
            <!DOCTYPE html>
            <html lang="en">
            	<head>
            		<title>%s</title>
            		<style>
            			.c{text-align: center;}
                        body{text-align: center;}
            			div,input{padding:5px;font-size:1em;}
            			input{width:95%%;}
            			button{border:0;border-radius:0.3rem;background-color:#1fa3ec;color:#fff;line-height:2.4rem;font-size:1.2rem;width:100%%;}
                        hr.dotted { border-top: 1px dotted darkblue;}
                        hr.thick { border: 3px solid lightblue; border-radius: 2px; }
            			.q{float: right;width: 64px;text-align: right;}
            			.l{background: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAALVBMVEX///8EBwfBwsLw8PAzNjaCg4NTVVUjJiZDRUUUFxdiZGSho6OSk5Pg4eFydHTCjaf3AAAAZElEQVQ4je2NSw7AIAhEBamKn97/uMXEGBvozkWb9C2Zx4xzWykBhFAeYp9gkLyZE0zIMno9n4g19hmdY39scwqVkOXaxph0ZCXQcqxSpgQpONa59wkRDOL93eAXvimwlbPbwwVAegLS1HGfZAAAAABJRU5ErkJggg==") no-repeat left center;background-size: 1em;}
                        .footer {
                           position: fixed; left: 0; bottom: 0; width: 100%%; height=30;
                           background-color: #e1e1e1; color: #214682;}
            		</style>
            	</head>
            	<body>
            		<div style="text-align:left;display:inline-block;min-width:260px;">
            			<p align="center"> <font size="5" color="#ffffff"><b>%s</b></font> </p>
            			<br/>
        ''' % (self.title, self.title)

    def footer(self):
        return '''
            </div>
            <div class="footer">
                <span style="float:center;">%s</span>
            </div>
            </body>
            </html>
        ''' % (self.status_text)
