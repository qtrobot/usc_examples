

This example uses simple python web library called CherryPy (https://pypi.org/project/CherryPy/)

# Installation
NOTICE: CherryPy is already installed on the Nuc of QTrobot

if you want to try on another PC, install it using `pip`
```
pip install CherryPy
```

# running the qt_web_app demo server
```
$ cd qt_web_app
$  python qt_web_app.py
```

# opening web app interface
Open the following link in your browser: http://127.0.0.1:5000
You can move the robot head to left/right using web interface
