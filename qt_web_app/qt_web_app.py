"""
QTrobot webapp example
"""
import sys
import rospy
from std_msgs.msg import Float64MultiArray

import cherrypy
from qt_page import Page


class MainPage(Page):
    @cherrypy.expose
    def index(self):
        self.setStatusText("readys")
        return self.header() + '''
            <br/><form action="Look" method="get"><button type="submit" name="direction" value="right">Look Right</button></form>
            <br/><form action="Look" method="get"><button type="submit" name="direction" value="left">Look Left</button></form><br/>
        ''' + self.footer()

    @cherrypy.expose
    def Look(self, direction=None):
        if direction:
            self.setStatusText("Looking at %s" % direction)
            try:
                ref = Float64MultiArray()
                if direction == "right":
                    ref.data = [-45, 0]
                else:
                    ref.data = [45, 0]
                head_pub.publish(ref)
            except rospy.ROSInterruptException:
                self.setStatusText("Error: could not publish motor commnad!")
        else:
            self.setStatusText("Error: missing 'direction' value")

        return self.header() + '''
            <br/><form action="Look" method="get"><button type="submit" name="direction" value="right">Look Right</button></form>
            <br/><form action="Look" method="get"><button type="submit" name="direction" value="left">Look Left</button></form><br/>
        ''' + self.footer()


if __name__ == '__main__':
    rospy.init_node('joints_command_example')

    # create a publisher
    head_pub = rospy.Publisher('/qt_robot/head_position/command', Float64MultiArray, queue_size=1)

    # wait for publisher/subscriber connections
    wtime_begin = rospy.get_time()
    while (head_pub.get_num_connections() == 0) :
        rospy.loginfo("waiting for subscriber connections")
        if rospy.get_time() - wtime_begin > 10.0:
            rospy.logerr("Timeout while waiting for subscribers connection!")
            sys.exit()
        rospy.sleep(1)

    webconf = {
        'global': {
            'server.socket_host': '0.0.0.0',
            'server.socket_port': 5000,
            'server.thread_pool': 10
            }
    }

    root = MainPage()
    cherrypy.quickstart(root, "/", config=webconf)
